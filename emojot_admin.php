<?php
/**
 * Created by PhpStorm.
 * User: ASUS
 * Date: 6/13/2015
 * Time: 10:10 AM
 */
$hidden_flag = '';
if (isset($_POST['emojot_hidden'])){
    $hidden_flag = $_POST['emojot_hidden'];
}

$embed_code = '';
if (isset($_POST['emojot_embed_code'])){
    $embed_code = $_POST['emojot_embed_code'];
}
if($hidden_flag == 'Y') {
	update_option('emojot_embed_code', $embed_code);
	?>
	<div class="updated"><p><strong><?php _e('Options saved.' ); ?></strong></p></div>
<?php
} else {
	//Normal page display
	$embed_code = get_option('emojot_embed_code');
}
?>
<div class="wrap">
    <?php    echo "<h2>" . __( 'Emojot Settings', 'emojot_trdom' ) . "</h2>"; ?>
	<br class="clearFixx"/>
<form name="emojot_form" method="post" action="<?php echo str_replace( '%7E', '~', $_SERVER['REQUEST_URI']); ?>">
	<input type="hidden" name="emojot_hidden" value="Y">
	<div class="cell20"><?php _e("Embed Code: " ); ?></div>
	<div class="cell50" style="height: 75px;"><textarea rows="4" cols="100" name="emojot_embed_code" size="20"><?php echo $embed_code; ?></textarea></div>
	<br class="clearFixx"/>
	<div class="cell20"></div>
	<div class="cell50">
		<?php _e("To extract the embed code visit ")?><a href="http://emojot.com/registration" target="_blank"> here</a><?php _e(" and register yourself")?>
	</div>
	<br class="clearFixx"/>
	<hr />
	<p class="submit">
		<input type="submit" name="Submit" value="<?php _e('Update Options', 'emojot_trdom' ) ?>" />
	</p>
</form>
</div>

