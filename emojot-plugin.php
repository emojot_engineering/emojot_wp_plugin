<?php

/*
Plugin Name: Emojot
Plugin URI: http://URI_Of_Page_Describing_Plugin_and_Updates
Description: A brief description of the Plugin.
Version: 1.0
Author: ASUS
Author URI: http://URI_Of_The_Plugin_Author
License: A "Slug" license name e.g. GPL2
*/

function emojot_admin(){
	include('emojot_admin.php');

}
function emojot_admin_actions() {
	add_options_page("Emojot", "Emojot", "administrator", "Emojot", "emojot_admin");
}
function load_emojot_scripts() {
	// Register the script like this for a plugin:
	wp_register_style( 'emojot-css', plugins_url( '/css/emojot.css', __FILE__ ) );
	wp_enqueue_style( 'emojot-css' );
}

function load_emoter_ui($attr){
    $params = shortcode_atts( array(
        'type' => 'button',
        'id' => ''
    ), $attr );
    $ids = explode("_",$params['id']);
    $company_id = $ids[0];
    $sensor_id = $ids[1];
    $type = $params['type'];
    $retval = "";
    if($type == "inline"){
        $retval .= "<iframe style='width:100%;height:100%' src='https://emojot.com/mobile/mobile.jsp?loginRequest=Anonymous&widget=true&eventHash=";
        $retval .= $sensor_id;
        $retval .= "&companyHash=";
        $retval .= $company_id;
        $retval .= "' frameborder='0' allowfullscreen></iframe>";
    }else if($type == "button"){
        $retval = '<div class="emojot_balloon" id="';
        $retval .= $params['id'];
        $retval .='"></div>';
        $retval .= '<script src="https://emojot.com/integration/emojot_embed.js" type="text/javascript" charset="utf-8"></script>';
    }
    return $retval;
}
// Note : settings page will be added in future versions
//add_action( 'admin_init', 'load_emojot_scripts' );
//add_action('admin_menu', 'emojot_admin_actions');
add_shortcode( 'emojot', 'load_emoter_ui' );
