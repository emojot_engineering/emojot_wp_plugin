﻿=== Emojot - Instant Audience Feedback ===
Contributors: Emojot
Donate link: none
Tags: feedback,perception,audience
Requires at least: 3.0.1
Tested up to: 4.2.2
Stable tag: 1.0.0
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

== Description ==

Emojot is a revolutionary audience engagement product that enables dynamic two-way conversations with clients based on real-time perception analytics.
Emojot uses Emojis to capture emotions in a language-neutral manner and incorporates instant gratification to keep the audience highly engaged and 
Emojot empowers market insight with instant perception aggregation. Emojot is useful in enabling conversations across a multitude of scenarios ranging 
from events, reality TV shows, political campaigns, news programs, competitions and targeted marketing campaigns.This plug-in is to integrate Emojot 
with Wordpress websites.

== Installation ==
	
**First register with Emojot platform (emojot.com/registration)

**Then you can create your own sensor which is to be integrated with your Wordpress website. 
Add a name, start and end dates and url for the sensor. In the “Sensor Display”, tab you can add up to five different questions to get the viewers'
perceptions. For each question select a theme which consists of an array of Emojis and a text for each Emoji. There are inbuilt themes, but you can add more 
by visiting Settings -> Emote page.

**Then go to "Manage Sensors" tab and click on "Embed" on left bottom of the selected sensor.

** Select "Wordpress" as the embed option and copy the shortcode.

**Then go to the dashboard and install Emojot plug-in and activate it.

**Paste the short code in anyware in the page/post. You can add as many as you like for a particular page/post.

**Site administrator can see the analytics of the perception sensor using the Emojot dashboard.
		
== Frequently Asked Questions ==

Why is Emojot WP pluging gives you a unique experience in collecting audience perceptions?
	because Emojot,
		Can track perception of website audience by time, location, gender, age or other demographic information.
		Can be used to Add multiple questions to get more insights of audience perceptions.
		is a non obstructive perception capturing using extremely simple Emoji based perception themes.
		is highly customizable perception sensor which enables social media integration and make analytics publicly available.
		Can be used to enable textual feedback if needed.
		
== Changelog ==
= 1.0.0 =
**initial release

== Upgrade Notice ==
To be updated

== Screenshots ==
To be updated